import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetoAutorizacoesPredioTestModule } from '../../../test.module';
import { AutorizacaoComponent } from 'app/entities/autorizacao/autorizacao.component';
import { AutorizacaoService } from 'app/entities/autorizacao/autorizacao.service';
import { Autorizacao } from 'app/shared/model/autorizacao.model';

describe('Component Tests', () => {
  describe('Autorizacao Management Component', () => {
    let comp: AutorizacaoComponent;
    let fixture: ComponentFixture<AutorizacaoComponent>;
    let service: AutorizacaoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetoAutorizacoesPredioTestModule],
        declarations: [AutorizacaoComponent],
        providers: []
      })
        .overrideTemplate(AutorizacaoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AutorizacaoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AutorizacaoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Autorizacao(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.autorizacaos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
