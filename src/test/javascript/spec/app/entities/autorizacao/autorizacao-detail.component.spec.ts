import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetoAutorizacoesPredioTestModule } from '../../../test.module';
import { AutorizacaoDetailComponent } from 'app/entities/autorizacao/autorizacao-detail.component';
import { Autorizacao } from 'app/shared/model/autorizacao.model';

describe('Component Tests', () => {
  describe('Autorizacao Management Detail Component', () => {
    let comp: AutorizacaoDetailComponent;
    let fixture: ComponentFixture<AutorizacaoDetailComponent>;
    const route = ({ data: of({ autorizacao: new Autorizacao(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetoAutorizacoesPredioTestModule],
        declarations: [AutorizacaoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AutorizacaoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AutorizacaoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.autorizacao).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
