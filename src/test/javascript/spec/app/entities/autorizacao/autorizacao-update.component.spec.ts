import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetoAutorizacoesPredioTestModule } from '../../../test.module';
import { AutorizacaoUpdateComponent } from 'app/entities/autorizacao/autorizacao-update.component';
import { AutorizacaoService } from 'app/entities/autorizacao/autorizacao.service';
import { Autorizacao } from 'app/shared/model/autorizacao.model';

describe('Component Tests', () => {
  describe('Autorizacao Management Update Component', () => {
    let comp: AutorizacaoUpdateComponent;
    let fixture: ComponentFixture<AutorizacaoUpdateComponent>;
    let service: AutorizacaoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetoAutorizacoesPredioTestModule],
        declarations: [AutorizacaoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AutorizacaoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AutorizacaoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AutorizacaoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Autorizacao(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Autorizacao();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
