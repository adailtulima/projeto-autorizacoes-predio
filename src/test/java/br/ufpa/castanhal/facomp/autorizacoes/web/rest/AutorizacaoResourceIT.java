package br.ufpa.castanhal.facomp.autorizacoes.web.rest;

import br.ufpa.castanhal.facomp.autorizacoes.ProjetoAutorizacoesPredioApp;
import br.ufpa.castanhal.facomp.autorizacoes.domain.Autorizacao;
import br.ufpa.castanhal.facomp.autorizacoes.repository.AutorizacaoRepository;
import br.ufpa.castanhal.facomp.autorizacoes.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static br.ufpa.castanhal.facomp.autorizacoes.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AutorizacaoResource} REST controller.
 */
@SpringBootTest(classes = ProjetoAutorizacoesPredioApp.class)
public class AutorizacaoResourceIT {

    private static final LocalDate DEFAULT_DATA_INICIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INICIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATA_FIM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FIM = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MOTIVO = "AAAAAAAAAA";
    private static final String UPDATED_MOTIVO = "BBBBBBBBBB";

    @Autowired
    private AutorizacaoRepository autorizacaoRepository;

    @Mock
    private AutorizacaoRepository autorizacaoRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAutorizacaoMockMvc;

    private Autorizacao autorizacao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AutorizacaoResource autorizacaoResource = new AutorizacaoResource(autorizacaoRepository);
        this.restAutorizacaoMockMvc = MockMvcBuilders.standaloneSetup(autorizacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Autorizacao createEntity(EntityManager em) {
        Autorizacao autorizacao = new Autorizacao()
            .dataInicio(DEFAULT_DATA_INICIO)
            .dataFim(DEFAULT_DATA_FIM)
            .motivo(DEFAULT_MOTIVO);
        return autorizacao;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Autorizacao createUpdatedEntity(EntityManager em) {
        Autorizacao autorizacao = new Autorizacao()
            .dataInicio(UPDATED_DATA_INICIO)
            .dataFim(UPDATED_DATA_FIM)
            .motivo(UPDATED_MOTIVO);
        return autorizacao;
    }

    @BeforeEach
    public void initTest() {
        autorizacao = createEntity(em);
    }

    @Test
    @Transactional
    public void createAutorizacao() throws Exception {
        int databaseSizeBeforeCreate = autorizacaoRepository.findAll().size();

        // Create the Autorizacao
        restAutorizacaoMockMvc.perform(post("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isCreated());

        // Validate the Autorizacao in the database
        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeCreate + 1);
        Autorizacao testAutorizacao = autorizacaoList.get(autorizacaoList.size() - 1);
        assertThat(testAutorizacao.getDataInicio()).isEqualTo(DEFAULT_DATA_INICIO);
        assertThat(testAutorizacao.getDataFim()).isEqualTo(DEFAULT_DATA_FIM);
        assertThat(testAutorizacao.getMotivo()).isEqualTo(DEFAULT_MOTIVO);
    }

    @Test
    @Transactional
    public void createAutorizacaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = autorizacaoRepository.findAll().size();

        // Create the Autorizacao with an existing ID
        autorizacao.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAutorizacaoMockMvc.perform(post("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isBadRequest());

        // Validate the Autorizacao in the database
        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDataInicioIsRequired() throws Exception {
        int databaseSizeBeforeTest = autorizacaoRepository.findAll().size();
        // set the field null
        autorizacao.setDataInicio(null);

        // Create the Autorizacao, which fails.

        restAutorizacaoMockMvc.perform(post("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isBadRequest());

        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataFimIsRequired() throws Exception {
        int databaseSizeBeforeTest = autorizacaoRepository.findAll().size();
        // set the field null
        autorizacao.setDataFim(null);

        // Create the Autorizacao, which fails.

        restAutorizacaoMockMvc.perform(post("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isBadRequest());

        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMotivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = autorizacaoRepository.findAll().size();
        // set the field null
        autorizacao.setMotivo(null);

        // Create the Autorizacao, which fails.

        restAutorizacaoMockMvc.perform(post("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isBadRequest());

        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAutorizacaos() throws Exception {
        // Initialize the database
        autorizacaoRepository.saveAndFlush(autorizacao);

        // Get all the autorizacaoList
        restAutorizacaoMockMvc.perform(get("/api/autorizacaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(autorizacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataInicio").value(hasItem(DEFAULT_DATA_INICIO.toString())))
            .andExpect(jsonPath("$.[*].dataFim").value(hasItem(DEFAULT_DATA_FIM.toString())))
            .andExpect(jsonPath("$.[*].motivo").value(hasItem(DEFAULT_MOTIVO)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAutorizacaosWithEagerRelationshipsIsEnabled() throws Exception {
        AutorizacaoResource autorizacaoResource = new AutorizacaoResource(autorizacaoRepositoryMock);
        when(autorizacaoRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restAutorizacaoMockMvc = MockMvcBuilders.standaloneSetup(autorizacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAutorizacaoMockMvc.perform(get("/api/autorizacaos?eagerload=true"))
        .andExpect(status().isOk());

        verify(autorizacaoRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAutorizacaosWithEagerRelationshipsIsNotEnabled() throws Exception {
        AutorizacaoResource autorizacaoResource = new AutorizacaoResource(autorizacaoRepositoryMock);
            when(autorizacaoRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restAutorizacaoMockMvc = MockMvcBuilders.standaloneSetup(autorizacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAutorizacaoMockMvc.perform(get("/api/autorizacaos?eagerload=true"))
        .andExpect(status().isOk());

            verify(autorizacaoRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAutorizacao() throws Exception {
        // Initialize the database
        autorizacaoRepository.saveAndFlush(autorizacao);

        // Get the autorizacao
        restAutorizacaoMockMvc.perform(get("/api/autorizacaos/{id}", autorizacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(autorizacao.getId().intValue()))
            .andExpect(jsonPath("$.dataInicio").value(DEFAULT_DATA_INICIO.toString()))
            .andExpect(jsonPath("$.dataFim").value(DEFAULT_DATA_FIM.toString()))
            .andExpect(jsonPath("$.motivo").value(DEFAULT_MOTIVO));
    }

    @Test
    @Transactional
    public void getNonExistingAutorizacao() throws Exception {
        // Get the autorizacao
        restAutorizacaoMockMvc.perform(get("/api/autorizacaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAutorizacao() throws Exception {
        // Initialize the database
        autorizacaoRepository.saveAndFlush(autorizacao);

        int databaseSizeBeforeUpdate = autorizacaoRepository.findAll().size();

        // Update the autorizacao
        Autorizacao updatedAutorizacao = autorizacaoRepository.findById(autorizacao.getId()).get();
        // Disconnect from session so that the updates on updatedAutorizacao are not directly saved in db
        em.detach(updatedAutorizacao);
        updatedAutorizacao
            .dataInicio(UPDATED_DATA_INICIO)
            .dataFim(UPDATED_DATA_FIM)
            .motivo(UPDATED_MOTIVO);

        restAutorizacaoMockMvc.perform(put("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAutorizacao)))
            .andExpect(status().isOk());

        // Validate the Autorizacao in the database
        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeUpdate);
        Autorizacao testAutorizacao = autorizacaoList.get(autorizacaoList.size() - 1);
        assertThat(testAutorizacao.getDataInicio()).isEqualTo(UPDATED_DATA_INICIO);
        assertThat(testAutorizacao.getDataFim()).isEqualTo(UPDATED_DATA_FIM);
        assertThat(testAutorizacao.getMotivo()).isEqualTo(UPDATED_MOTIVO);
    }

    @Test
    @Transactional
    public void updateNonExistingAutorizacao() throws Exception {
        int databaseSizeBeforeUpdate = autorizacaoRepository.findAll().size();

        // Create the Autorizacao

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAutorizacaoMockMvc.perform(put("/api/autorizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(autorizacao)))
            .andExpect(status().isBadRequest());

        // Validate the Autorizacao in the database
        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAutorizacao() throws Exception {
        // Initialize the database
        autorizacaoRepository.saveAndFlush(autorizacao);

        int databaseSizeBeforeDelete = autorizacaoRepository.findAll().size();

        // Delete the autorizacao
        restAutorizacaoMockMvc.perform(delete("/api/autorizacaos/{id}", autorizacao.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Autorizacao> autorizacaoList = autorizacaoRepository.findAll();
        assertThat(autorizacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
