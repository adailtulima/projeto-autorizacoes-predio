package br.ufpa.castanhal.facomp.autorizacoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import br.ufpa.castanhal.facomp.autorizacoes.web.rest.TestUtil;

public class AutorizacaoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Autorizacao.class);
        Autorizacao autorizacao1 = new Autorizacao();
        autorizacao1.setId(1L);
        Autorizacao autorizacao2 = new Autorizacao();
        autorizacao2.setId(autorizacao1.getId());
        assertThat(autorizacao1).isEqualTo(autorizacao2);
        autorizacao2.setId(2L);
        assertThat(autorizacao1).isNotEqualTo(autorizacao2);
        autorizacao1.setId(null);
        assertThat(autorizacao1).isNotEqualTo(autorizacao2);
    }
}
