package br.ufpa.castanhal.facomp.autorizacoes.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import br.ufpa.castanhal.facomp.autorizacoes.domain.enumeration.Curso;

/**
 * A Aluno.
 */
@Entity
@Table(name = "aluno")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Aluno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "matricula")
    private String matricula;

    @Column(name = "turma")
    private String turma;

    @Column(name = "ano")
    private Integer ano;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "curso", nullable = false)
    private Curso curso;

    @ManyToMany(mappedBy = "alunos")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Autorizacao> autorizacoes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Aluno nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public Aluno cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getMatricula() {
        return matricula;
    }

    public Aluno matricula(String matricula) {
        this.matricula = matricula;
        return this;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTurma() {
        return turma;
    }

    public Aluno turma(String turma) {
        this.turma = turma;
        return this;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public Integer getAno() {
        return ano;
    }

    public Aluno ano(Integer ano) {
        this.ano = ano;
        return this;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Curso getCurso() {
        return curso;
    }

    public Aluno curso(Curso curso) {
        this.curso = curso;
        return this;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Set<Autorizacao> getAutorizacoes() {
        return autorizacoes;
    }

    public Aluno autorizacoes(Set<Autorizacao> autorizacaos) {
        this.autorizacoes = autorizacaos;
        return this;
    }

    public Aluno addAutorizacoes(Autorizacao autorizacao) {
        this.autorizacoes.add(autorizacao);
        autorizacao.getAlunos().add(this);
        return this;
    }

    public Aluno removeAutorizacoes(Autorizacao autorizacao) {
        this.autorizacoes.remove(autorizacao);
        autorizacao.getAlunos().remove(this);
        return this;
    }

    public void setAutorizacoes(Set<Autorizacao> autorizacaos) {
        this.autorizacoes = autorizacaos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Aluno)) {
            return false;
        }
        return id != null && id.equals(((Aluno) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Aluno{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", cpf='" + getCpf() + "'" +
            ", matricula='" + getMatricula() + "'" +
            ", turma='" + getTurma() + "'" +
            ", ano=" + getAno() +
            ", curso='" + getCurso() + "'" +
            "}";
    }
}
