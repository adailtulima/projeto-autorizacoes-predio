/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package br.ufpa.castanhal.facomp.autorizacoes.web.rest.errors;
