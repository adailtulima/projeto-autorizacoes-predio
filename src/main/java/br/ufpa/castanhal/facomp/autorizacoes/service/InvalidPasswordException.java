package br.ufpa.castanhal.facomp.autorizacoes.service;

public class InvalidPasswordException extends RuntimeException {

    public InvalidPasswordException() {
        super("Incorrect password");
    }

}
