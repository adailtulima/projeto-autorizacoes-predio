package br.ufpa.castanhal.facomp.autorizacoes.web.rest;

import br.ufpa.castanhal.facomp.autorizacoes.domain.Autorizacao;
import br.ufpa.castanhal.facomp.autorizacoes.repository.AutorizacaoRepository;
import br.ufpa.castanhal.facomp.autorizacoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.ufpa.castanhal.facomp.autorizacoes.domain.Autorizacao}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AutorizacaoResource {

    private final Logger log = LoggerFactory.getLogger(AutorizacaoResource.class);

    private static final String ENTITY_NAME = "autorizacao";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AutorizacaoRepository autorizacaoRepository;

    public AutorizacaoResource(AutorizacaoRepository autorizacaoRepository) {
        this.autorizacaoRepository = autorizacaoRepository;
    }

    /**
     * {@code POST  /autorizacaos} : Create a new autorizacao.
     *
     * @param autorizacao the autorizacao to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new autorizacao, or with status {@code 400 (Bad Request)} if the autorizacao has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/autorizacaos")
    public ResponseEntity<Autorizacao> createAutorizacao(@Valid @RequestBody Autorizacao autorizacao) throws URISyntaxException {
        log.debug("REST request to save Autorizacao : {}", autorizacao);
        if (autorizacao.getId() != null) {
            throw new BadRequestAlertException("A new autorizacao cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Autorizacao result = autorizacaoRepository.save(autorizacao);
        return ResponseEntity.created(new URI("/api/autorizacaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /autorizacaos} : Updates an existing autorizacao.
     *
     * @param autorizacao the autorizacao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated autorizacao,
     * or with status {@code 400 (Bad Request)} if the autorizacao is not valid,
     * or with status {@code 500 (Internal Server Error)} if the autorizacao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/autorizacaos")
    public ResponseEntity<Autorizacao> updateAutorizacao(@Valid @RequestBody Autorizacao autorizacao) throws URISyntaxException {
        log.debug("REST request to update Autorizacao : {}", autorizacao);
        if (autorizacao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Autorizacao result = autorizacaoRepository.save(autorizacao);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, autorizacao.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /autorizacaos} : get all the autorizacaos.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of autorizacaos in body.
     */
    @GetMapping("/autorizacaos")
    public List<Autorizacao> getAllAutorizacaos(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Autorizacaos");
        return autorizacaoRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /autorizacaos/:id} : get the "id" autorizacao.
     *
     * @param id the id of the autorizacao to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the autorizacao, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/autorizacaos/{id}")
    public ResponseEntity<Autorizacao> getAutorizacao(@PathVariable Long id) {
        log.debug("REST request to get Autorizacao : {}", id);
        Optional<Autorizacao> autorizacao = autorizacaoRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(autorizacao);
    }

    /**
     * {@code DELETE  /autorizacaos/:id} : delete the "id" autorizacao.
     *
     * @param id the id of the autorizacao to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/autorizacaos/{id}")
    public ResponseEntity<Void> deleteAutorizacao(@PathVariable Long id) {
        log.debug("REST request to delete Autorizacao : {}", id);
        autorizacaoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
