package br.ufpa.castanhal.facomp.autorizacoes.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Autorizacao.
 */
@Entity
@Table(name = "autorizacao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Autorizacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "data_inicio", nullable = false)
    private LocalDate dataInicio;

    @NotNull
    @Column(name = "data_fim", nullable = false)
    private LocalDate dataFim;

    @NotNull
    @Column(name = "motivo", nullable = false)
    private String motivo;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "autorizacao_alunos",
               joinColumns = @JoinColumn(name = "autorizacao_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "alunos_id", referencedColumnName = "id"))
    private Set<Aluno> alunos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("autorizacoes")
    private Professor professor;

    @ManyToOne
    @JsonIgnoreProperties("autorizacoes")
    private Sala sala;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public Autorizacao dataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
        return this;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public Autorizacao dataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
        return this;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public String getMotivo() {
        return motivo;
    }

    public Autorizacao motivo(String motivo) {
        this.motivo = motivo;
        return this;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Set<Aluno> getAlunos() {
        return alunos;
    }

    public Autorizacao alunos(Set<Aluno> alunos) {
        this.alunos = alunos;
        return this;
    }

    public Autorizacao addAlunos(Aluno aluno) {
        this.alunos.add(aluno);
        aluno.getAutorizacoes().add(this);
        return this;
    }

    public Autorizacao removeAlunos(Aluno aluno) {
        this.alunos.remove(aluno);
        aluno.getAutorizacoes().remove(this);
        return this;
    }

    public void setAlunos(Set<Aluno> alunos) {
        this.alunos = alunos;
    }

    public Professor getProfessor() {
        return professor;
    }

    public Autorizacao professor(Professor professor) {
        this.professor = professor;
        return this;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Sala getSala() {
        return sala;
    }

    public Autorizacao sala(Sala sala) {
        this.sala = sala;
        return this;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Autorizacao)) {
            return false;
        }
        return id != null && id.equals(((Autorizacao) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Autorizacao{" +
            "id=" + getId() +
            ", dataInicio='" + getDataInicio() + "'" +
            ", dataFim='" + getDataFim() + "'" +
            ", motivo='" + getMotivo() + "'" +
            "}";
    }
}
