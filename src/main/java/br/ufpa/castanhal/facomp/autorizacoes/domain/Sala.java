package br.ufpa.castanhal.facomp.autorizacoes.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Sala.
 */
@Entity
@Table(name = "sala")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sala implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @OneToMany(mappedBy = "sala")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Autorizacao> autorizacoes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Sala nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<Autorizacao> getAutorizacoes() {
        return autorizacoes;
    }

    public Sala autorizacoes(Set<Autorizacao> autorizacaos) {
        this.autorizacoes = autorizacaos;
        return this;
    }

    public Sala addAutorizacoes(Autorizacao autorizacao) {
        this.autorizacoes.add(autorizacao);
        autorizacao.setSala(this);
        return this;
    }

    public Sala removeAutorizacoes(Autorizacao autorizacao) {
        this.autorizacoes.remove(autorizacao);
        autorizacao.setSala(null);
        return this;
    }

    public void setAutorizacoes(Set<Autorizacao> autorizacaos) {
        this.autorizacoes = autorizacaos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sala)) {
            return false;
        }
        return id != null && id.equals(((Sala) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Sala{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            "}";
    }
}
