package br.ufpa.castanhal.facomp.autorizacoes.repository;
import br.ufpa.castanhal.facomp.autorizacoes.domain.Sala;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sala entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SalaRepository extends JpaRepository<Sala, Long> {

}
