package br.ufpa.castanhal.facomp.autorizacoes.repository;
import br.ufpa.castanhal.facomp.autorizacoes.domain.Autorizacao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Autorizacao entity.
 */
@Repository
public interface AutorizacaoRepository extends JpaRepository<Autorizacao, Long> {

    @Query(value = "select distinct autorizacao from Autorizacao autorizacao left join fetch autorizacao.alunos",
        countQuery = "select count(distinct autorizacao) from Autorizacao autorizacao")
    Page<Autorizacao> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct autorizacao from Autorizacao autorizacao left join fetch autorizacao.alunos")
    List<Autorizacao> findAllWithEagerRelationships();

    @Query("select autorizacao from Autorizacao autorizacao left join fetch autorizacao.alunos where autorizacao.id =:id")
    Optional<Autorizacao> findOneWithEagerRelationships(@Param("id") Long id);

}
