package br.ufpa.castanhal.facomp.autorizacoes.repository;

import br.ufpa.castanhal.facomp.autorizacoes.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
