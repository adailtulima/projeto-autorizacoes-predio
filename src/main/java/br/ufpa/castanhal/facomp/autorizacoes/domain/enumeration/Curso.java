package br.ufpa.castanhal.facomp.autorizacoes.domain.enumeration;

/**
 * The Curso enumeration.
 */
public enum Curso {
    SI, EC
}
