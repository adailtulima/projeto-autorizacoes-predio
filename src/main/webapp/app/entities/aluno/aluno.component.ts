import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAluno } from 'app/shared/model/aluno.model';
import { AlunoService } from './aluno.service';
import { AlunoDeleteDialogComponent } from './aluno-delete-dialog.component';

@Component({
  selector: 'jhi-aluno',
  templateUrl: './aluno.component.html'
})
export class AlunoComponent implements OnInit, OnDestroy {
  alunos: IAluno[];
  eventSubscriber: Subscription;

  constructor(protected alunoService: AlunoService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.alunoService.query().subscribe((res: HttpResponse<IAluno[]>) => {
      this.alunos = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInAlunos();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IAluno) {
    return item.id;
  }

  registerChangeInAlunos() {
    this.eventSubscriber = this.eventManager.subscribe('alunoListModification', () => this.loadAll());
  }

  delete(aluno: IAluno) {
    const modalRef = this.modalService.open(AlunoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.aluno = aluno;
  }
}
