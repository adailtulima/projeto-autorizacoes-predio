import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IAluno, Aluno } from 'app/shared/model/aluno.model';
import { AlunoService } from './aluno.service';
import { IAutorizacao } from 'app/shared/model/autorizacao.model';
import { AutorizacaoService } from 'app/entities/autorizacao/autorizacao.service';

@Component({
  selector: 'jhi-aluno-update',
  templateUrl: './aluno-update.component.html'
})
export class AlunoUpdateComponent implements OnInit {
  isSaving: boolean;

  autorizacaos: IAutorizacao[];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    cpf: [],
    matricula: [],
    turma: [],
    ano: [],
    curso: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected alunoService: AlunoService,
    protected autorizacaoService: AutorizacaoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ aluno }) => {
      this.updateForm(aluno);
    });
    this.autorizacaoService
      .query()
      .subscribe(
        (res: HttpResponse<IAutorizacao[]>) => (this.autorizacaos = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(aluno: IAluno) {
    this.editForm.patchValue({
      id: aluno.id,
      nome: aluno.nome,
      cpf: aluno.cpf,
      matricula: aluno.matricula,
      turma: aluno.turma,
      ano: aluno.ano,
      curso: aluno.curso
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const aluno = this.createFromForm();
    if (aluno.id !== undefined) {
      this.subscribeToSaveResponse(this.alunoService.update(aluno));
    } else {
      this.subscribeToSaveResponse(this.alunoService.create(aluno));
    }
  }

  private createFromForm(): IAluno {
    return {
      ...new Aluno(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      cpf: this.editForm.get(['cpf']).value,
      matricula: this.editForm.get(['matricula']).value,
      turma: this.editForm.get(['turma']).value,
      ano: this.editForm.get(['ano']).value,
      curso: this.editForm.get(['curso']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAluno>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackAutorizacaoById(index: number, item: IAutorizacao) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
