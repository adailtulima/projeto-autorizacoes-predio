import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'professor',
        loadChildren: () => import('./professor/professor.module').then(m => m.ProjetoAutorizacoesPredioProfessorModule)
      },
      {
        path: 'aluno',
        loadChildren: () => import('./aluno/aluno.module').then(m => m.ProjetoAutorizacoesPredioAlunoModule)
      },
      {
        path: 'sala',
        loadChildren: () => import('./sala/sala.module').then(m => m.ProjetoAutorizacoesPredioSalaModule)
      },
      {
        path: 'autorizacao',
        loadChildren: () => import('./autorizacao/autorizacao.module').then(m => m.ProjetoAutorizacoesPredioAutorizacaoModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class ProjetoAutorizacoesPredioEntityModule {}
