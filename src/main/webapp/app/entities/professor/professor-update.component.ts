import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IProfessor, Professor } from 'app/shared/model/professor.model';
import { ProfessorService } from './professor.service';

@Component({
  selector: 'jhi-professor-update',
  templateUrl: './professor-update.component.html'
})
export class ProfessorUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    siape: [],
    faculdade: []
  });

  constructor(protected professorService: ProfessorService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ professor }) => {
      this.updateForm(professor);
    });
  }

  updateForm(professor: IProfessor) {
    this.editForm.patchValue({
      id: professor.id,
      nome: professor.nome,
      siape: professor.siape,
      faculdade: professor.faculdade
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const professor = this.createFromForm();
    if (professor.id !== undefined) {
      this.subscribeToSaveResponse(this.professorService.update(professor));
    } else {
      this.subscribeToSaveResponse(this.professorService.create(professor));
    }
  }

  private createFromForm(): IProfessor {
    return {
      ...new Professor(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      siape: this.editForm.get(['siape']).value,
      faculdade: this.editForm.get(['faculdade']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfessor>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
