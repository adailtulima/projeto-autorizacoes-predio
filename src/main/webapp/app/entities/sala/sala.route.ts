import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sala } from 'app/shared/model/sala.model';
import { SalaService } from './sala.service';
import { SalaComponent } from './sala.component';
import { SalaDetailComponent } from './sala-detail.component';
import { SalaUpdateComponent } from './sala-update.component';
import { ISala } from 'app/shared/model/sala.model';

@Injectable({ providedIn: 'root' })
export class SalaResolve implements Resolve<ISala> {
  constructor(private service: SalaService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISala> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((sala: HttpResponse<Sala>) => sala.body));
    }
    return of(new Sala());
  }
}

export const salaRoute: Routes = [
  {
    path: '',
    component: SalaComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.sala.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SalaDetailComponent,
    resolve: {
      sala: SalaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.sala.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SalaUpdateComponent,
    resolve: {
      sala: SalaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.sala.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SalaUpdateComponent,
    resolve: {
      sala: SalaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.sala.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
