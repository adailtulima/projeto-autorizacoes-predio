import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAutorizacao } from 'app/shared/model/autorizacao.model';
import { AutorizacaoService } from './autorizacao.service';
import { AutorizacaoDeleteDialogComponent } from './autorizacao-delete-dialog.component';

@Component({
  selector: 'jhi-autorizacao',
  templateUrl: './autorizacao.component.html'
})
export class AutorizacaoComponent implements OnInit, OnDestroy {
  autorizacaos: IAutorizacao[];
  eventSubscriber: Subscription;

  constructor(
    protected autorizacaoService: AutorizacaoService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll() {
    this.autorizacaoService.query().subscribe((res: HttpResponse<IAutorizacao[]>) => {
      this.autorizacaos = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInAutorizacaos();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IAutorizacao) {
    return item.id;
  }

  registerChangeInAutorizacaos() {
    this.eventSubscriber = this.eventManager.subscribe('autorizacaoListModification', () => this.loadAll());
  }

  delete(autorizacao: IAutorizacao) {
    const modalRef = this.modalService.open(AutorizacaoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.autorizacao = autorizacao;
  }
}
