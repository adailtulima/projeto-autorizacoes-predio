import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IAutorizacao, Autorizacao } from 'app/shared/model/autorizacao.model';
import { AutorizacaoService } from './autorizacao.service';
import { IAluno } from 'app/shared/model/aluno.model';
import { AlunoService } from 'app/entities/aluno/aluno.service';
import { IProfessor } from 'app/shared/model/professor.model';
import { ProfessorService } from 'app/entities/professor/professor.service';
import { ISala } from 'app/shared/model/sala.model';
import { SalaService } from 'app/entities/sala/sala.service';

@Component({
  selector: 'jhi-autorizacao-update',
  templateUrl: './autorizacao-update.component.html'
})
export class AutorizacaoUpdateComponent implements OnInit {
  isSaving: boolean;

  alunos: IAluno[];

  professors: IProfessor[];

  salas: ISala[];
  dataInicioDp: any;
  dataFimDp: any;

  editForm = this.fb.group({
    id: [],
    dataInicio: [null, [Validators.required]],
    dataFim: [null, [Validators.required]],
    motivo: [null, [Validators.required]],
    alunos: [],
    professor: [],
    sala: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected autorizacaoService: AutorizacaoService,
    protected alunoService: AlunoService,
    protected professorService: ProfessorService,
    protected salaService: SalaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ autorizacao }) => {
      this.updateForm(autorizacao);
    });
    this.alunoService
      .query()
      .subscribe((res: HttpResponse<IAluno[]>) => (this.alunos = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.professorService
      .query()
      .subscribe((res: HttpResponse<IProfessor[]>) => (this.professors = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.salaService
      .query()
      .subscribe((res: HttpResponse<ISala[]>) => (this.salas = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(autorizacao: IAutorizacao) {
    this.editForm.patchValue({
      id: autorizacao.id,
      dataInicio: autorizacao.dataInicio,
      dataFim: autorizacao.dataFim,
      motivo: autorizacao.motivo,
      alunos: autorizacao.alunos,
      professor: autorizacao.professor,
      sala: autorizacao.sala
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const autorizacao = this.createFromForm();
    if (autorizacao.id !== undefined) {
      this.subscribeToSaveResponse(this.autorizacaoService.update(autorizacao));
    } else {
      this.subscribeToSaveResponse(this.autorizacaoService.create(autorizacao));
    }
  }

  private createFromForm(): IAutorizacao {
    return {
      ...new Autorizacao(),
      id: this.editForm.get(['id']).value,
      dataInicio: this.editForm.get(['dataInicio']).value,
      dataFim: this.editForm.get(['dataFim']).value,
      motivo: this.editForm.get(['motivo']).value,
      alunos: this.editForm.get(['alunos']).value,
      professor: this.editForm.get(['professor']).value,
      sala: this.editForm.get(['sala']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAutorizacao>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackAlunoById(index: number, item: IAluno) {
    return item.id;
  }

  trackProfessorById(index: number, item: IProfessor) {
    return item.id;
  }

  trackSalaById(index: number, item: ISala) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
