import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetoAutorizacoesPredioSharedModule } from 'app/shared/shared.module';
import { AutorizacaoComponent } from './autorizacao.component';
import { AutorizacaoDetailComponent } from './autorizacao-detail.component';
import { AutorizacaoUpdateComponent } from './autorizacao-update.component';
import { AutorizacaoDeleteDialogComponent } from './autorizacao-delete-dialog.component';
import { autorizacaoRoute } from './autorizacao.route';

@NgModule({
  imports: [ProjetoAutorizacoesPredioSharedModule, RouterModule.forChild(autorizacaoRoute)],
  declarations: [AutorizacaoComponent, AutorizacaoDetailComponent, AutorizacaoUpdateComponent, AutorizacaoDeleteDialogComponent],
  entryComponents: [AutorizacaoDeleteDialogComponent]
})
export class ProjetoAutorizacoesPredioAutorizacaoModule {}
