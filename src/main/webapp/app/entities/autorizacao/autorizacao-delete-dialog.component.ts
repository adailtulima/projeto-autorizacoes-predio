import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAutorizacao } from 'app/shared/model/autorizacao.model';
import { AutorizacaoService } from './autorizacao.service';

@Component({
  templateUrl: './autorizacao-delete-dialog.component.html'
})
export class AutorizacaoDeleteDialogComponent {
  autorizacao: IAutorizacao;

  constructor(
    protected autorizacaoService: AutorizacaoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.autorizacaoService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'autorizacaoListModification',
        content: 'Deleted an autorizacao'
      });
      this.activeModal.dismiss(true);
    });
  }
}
