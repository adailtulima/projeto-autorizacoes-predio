import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAutorizacao } from 'app/shared/model/autorizacao.model';

type EntityResponseType = HttpResponse<IAutorizacao>;
type EntityArrayResponseType = HttpResponse<IAutorizacao[]>;

@Injectable({ providedIn: 'root' })
export class AutorizacaoService {
  public resourceUrl = SERVER_API_URL + 'api/autorizacaos';

  constructor(protected http: HttpClient) {}

  create(autorizacao: IAutorizacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(autorizacao);
    return this.http
      .post<IAutorizacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(autorizacao: IAutorizacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(autorizacao);
    return this.http
      .put<IAutorizacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAutorizacao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAutorizacao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(autorizacao: IAutorizacao): IAutorizacao {
    const copy: IAutorizacao = Object.assign({}, autorizacao, {
      dataInicio: autorizacao.dataInicio != null && autorizacao.dataInicio.isValid() ? autorizacao.dataInicio.format(DATE_FORMAT) : null,
      dataFim: autorizacao.dataFim != null && autorizacao.dataFim.isValid() ? autorizacao.dataFim.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataInicio = res.body.dataInicio != null ? moment(res.body.dataInicio) : null;
      res.body.dataFim = res.body.dataFim != null ? moment(res.body.dataFim) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((autorizacao: IAutorizacao) => {
        autorizacao.dataInicio = autorizacao.dataInicio != null ? moment(autorizacao.dataInicio) : null;
        autorizacao.dataFim = autorizacao.dataFim != null ? moment(autorizacao.dataFim) : null;
      });
    }
    return res;
  }
}
