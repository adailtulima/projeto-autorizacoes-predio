import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAutorizacao } from 'app/shared/model/autorizacao.model';

@Component({
  selector: 'jhi-autorizacao-detail',
  templateUrl: './autorizacao-detail.component.html'
})
export class AutorizacaoDetailComponent implements OnInit {
  autorizacao: IAutorizacao;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ autorizacao }) => {
      this.autorizacao = autorizacao;
    });
  }

  previousState() {
    window.history.back();
  }
}
