import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Autorizacao } from 'app/shared/model/autorizacao.model';
import { AutorizacaoService } from './autorizacao.service';
import { AutorizacaoComponent } from './autorizacao.component';
import { AutorizacaoDetailComponent } from './autorizacao-detail.component';
import { AutorizacaoUpdateComponent } from './autorizacao-update.component';
import { IAutorizacao } from 'app/shared/model/autorizacao.model';

@Injectable({ providedIn: 'root' })
export class AutorizacaoResolve implements Resolve<IAutorizacao> {
  constructor(private service: AutorizacaoService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAutorizacao> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((autorizacao: HttpResponse<Autorizacao>) => autorizacao.body));
    }
    return of(new Autorizacao());
  }
}

export const autorizacaoRoute: Routes = [
  {
    path: '',
    component: AutorizacaoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.autorizacao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AutorizacaoDetailComponent,
    resolve: {
      autorizacao: AutorizacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.autorizacao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AutorizacaoUpdateComponent,
    resolve: {
      autorizacao: AutorizacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.autorizacao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AutorizacaoUpdateComponent,
    resolve: {
      autorizacao: AutorizacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'projetoAutorizacoesPredioApp.autorizacao.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
