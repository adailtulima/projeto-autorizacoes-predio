import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProjetoAutorizacoesPredioSharedModule } from 'app/shared/shared.module';

import { JhiConfigurationComponent } from './configuration.component';

import { configurationRoute } from './configuration.route';

@NgModule({
  imports: [ProjetoAutorizacoesPredioSharedModule, RouterModule.forChild([configurationRoute])],
  declarations: [JhiConfigurationComponent]
})
export class ConfigurationModule {}
