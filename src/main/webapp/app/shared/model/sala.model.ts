import { IAutorizacao } from 'app/shared/model/autorizacao.model';

export interface ISala {
  id?: number;
  nome?: string;
  autorizacoes?: IAutorizacao[];
}

export class Sala implements ISala {
  constructor(public id?: number, public nome?: string, public autorizacoes?: IAutorizacao[]) {}
}
