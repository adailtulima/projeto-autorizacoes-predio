import { IAutorizacao } from 'app/shared/model/autorizacao.model';

export interface IProfessor {
  id?: number;
  nome?: string;
  siape?: string;
  faculdade?: string;
  autorizacoes?: IAutorizacao[];
}

export class Professor implements IProfessor {
  constructor(
    public id?: number,
    public nome?: string,
    public siape?: string,
    public faculdade?: string,
    public autorizacoes?: IAutorizacao[]
  ) {}
}
