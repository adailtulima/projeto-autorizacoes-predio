import { IAutorizacao } from 'app/shared/model/autorizacao.model';
import { Curso } from 'app/shared/model/enumerations/curso.model';

export interface IAluno {
  id?: number;
  nome?: string;
  cpf?: string;
  matricula?: string;
  turma?: string;
  ano?: number;
  curso?: Curso;
  autorizacoes?: IAutorizacao[];
}

export class Aluno implements IAluno {
  constructor(
    public id?: number,
    public nome?: string,
    public cpf?: string,
    public matricula?: string,
    public turma?: string,
    public ano?: number,
    public curso?: Curso,
    public autorizacoes?: IAutorizacao[]
  ) {}
}
