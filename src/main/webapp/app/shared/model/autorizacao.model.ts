import { Moment } from 'moment';
import { IAluno } from 'app/shared/model/aluno.model';
import { IProfessor } from 'app/shared/model/professor.model';
import { ISala } from 'app/shared/model/sala.model';

export interface IAutorizacao {
  id?: number;
  dataInicio?: Moment;
  dataFim?: Moment;
  motivo?: string;
  alunos?: IAluno[];
  professor?: IProfessor;
  sala?: ISala;
}

export class Autorizacao implements IAutorizacao {
  constructor(
    public id?: number,
    public dataInicio?: Moment,
    public dataFim?: Moment,
    public motivo?: string,
    public alunos?: IAluno[],
    public professor?: IProfessor,
    public sala?: ISala
  ) {}
}
